from flask import Flask, jsonify, request
import os

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))  # __file__ means my current script


@app.route('/')
def home():
    return jsonify(data='Welcome!!!')



if __name__ == '__main__':
    #
    app.run()